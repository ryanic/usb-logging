﻿Read-Host "Insert USB and press enter"
Write-Host "" -ForegroundColor Magenta
Get-WinEvent -LogName Microsoft-Windows-DriverFrameworks-UserMode/Operational -MaxEvents 5 | % {$_; $_.Message}
Read-Host "Press enter to disable USB logging and clear logs"