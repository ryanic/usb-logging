Write-Host "Disabling DriverFrameworks-UserMode Logging" -ForegroundColor Magenta

# https://www.powershellmagazine.com/2013/07/15/pstip-how-to-enable-event-logs-using-windows-powershell/
$logName = 'Microsoft-Windows-DriverFrameworks-UserMode/Operational'
$log = New-Object System.Diagnostics.Eventing.Reader.EventLogConfiguration $logName
$log.IsEnabled=$false
$log.SaveChanges()

Write-Host "Clearing all event logs..." -ForegroundColor Magenta
wevtutil el | Foreach-Object {wevtutil cl �$_�}