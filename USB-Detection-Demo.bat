@echo off
echo "Enabling USB Logging"
powershell -exec bypass -c "& '%~dp0\PowerShell\Enable-USBLogging.ps1'"
powershell -exec bypass -c "& '%~dp0\PowerShell\USB-PromptandLog.ps1'"
powershell -exec bypass -c "& '%~dp0\PowerShell\Disable-USBLogging.ps1'"
pause